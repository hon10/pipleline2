using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using System;

using static SkillData;

public class StorePage : MonoBehaviour
{
    [Serializable]
    public struct StoreItem
    {
        public string Name;
        public Sprite Image;
        public int Price;
    }

    [SerializeField] private PageRoot _pageRoot;

    [SerializeField] private Button _toNextPhaseBtn;


    [SerializeField] private StoreItemGroup _storeItemGroupPrefab;
    [SerializeField] private RectTransform _storeItemGroupRoot;
    [SerializeField] private ScrollRect _scrollRect;

    [Header("StoreItem")]
    [SerializeField] private Sprite _drawingIcon;
    [SerializeField] private Sprite _cyclingIcon;
    [SerializeField] private Sprite _pianoIcon;
    [SerializeField] private Sprite _basketballIcon;
    [SerializeField] private StoreItem[] _drawingStoreItems = new StoreItem[5];
    [SerializeField] private StoreItem[] _cyclingStoreItems = new StoreItem[5];
    [SerializeField] private StoreItem[] _pianoStoreItems = new StoreItem[5];
    [SerializeField] private StoreItem[] _basketballStoreItems = new StoreItem[5];


    private List<StoreItemGroup> _storeItemGroups = new List<StoreItemGroup>();

    public void Init()
    {
    }

    public async UniTask Flow(List<SkillType> skills)
    {
        _pageRoot.Show();

        _SetupStoreItemGroup(skills);

        // completed
        await _toNextPhaseBtn.onClick.OnInvokeAsync(new System.Threading.CancellationToken());

        _pageRoot.Hide();
    }

    private void _SetupStoreItemGroup(List<SkillType> skills)
    {
        _ClearSelectingGroup();

        foreach (var skill in skills)
        {
            var storeItemGroup = Instantiate(_storeItemGroupPrefab, _storeItemGroupRoot);

            switch (skill)
            {
                case SkillType.Drawing:
                    storeItemGroup.Init(_drawingIcon, _drawingStoreItems);
                    break;
                case SkillType.Cycling:
                    storeItemGroup.Init(_cyclingIcon, _cyclingStoreItems);
                    break;
                case SkillType.Piano:
                    storeItemGroup.Init(_pianoIcon, _pianoStoreItems);
                    break;
                case SkillType.Basketball:
                    storeItemGroup.Init(_basketballIcon, _basketballStoreItems);
                    break;
            }

            _storeItemGroups.Add(storeItemGroup);
        }
    }

    private void _ClearSelectingGroup()
    {
        foreach (var item in _storeItemGroups)
            Destroy(item.gameObject);

        _storeItemGroups.Clear();
    }

}
