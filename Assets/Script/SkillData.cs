using System.Collections;
using System.Collections.Generic;

public class SkillData
{
    public enum SkillType
    {
        Drawing, Cycling, Piano, Basketball
    }

    public class Skill
    {
        public SkillType Type;
        public string Title;
        public string Description;
        public List<string> Quests = new List<string>();
        public List<int> Bonuses = new List<int>();

        public Skill(SkillType type, string title,
            string description,
            string quest1, string quest2, string quest3,
            int bonus1, int bonus2, int bonus3)
        {
            Type = type;
            Title = title;
            Description = description;

            Quests.Add(quest1);
            Quests.Add(quest2);
            Quests.Add(quest3);

            Bonuses.Add(bonus1);
            Bonuses.Add(bonus2);
            Bonuses.Add(bonus3);
        }
    }

    public static Dictionary<SkillType, Skill> MockData => _GetMockData();

    private static Dictionary<SkillType, Skill> _GetMockData()
    {
        var mockData = new Dictionary<SkillType, Skill>();

        mockData.Add(SkillType.Drawing,
            new Skill(SkillType.Drawing, "Drawing",
                "Emotion diffused. Discovery of one-self. Expression of thoughts. Delve deeper into drawing, and create your own imagination through eloquent visualization.",
                quest1: "Find 5 objects around and sketch them",
                quest2: "Find a person and sketch them",
                quest3: "Find complicated object and break them into easy shapes before sketching it",
                 150, 78, 245
            ));

        mockData.Add(SkillType.Cycling,
            new Skill(SkillType.Cycling, "Cycling",
                "Breathtaking scenery. Calming breeze. Take your bicycle, and experience the best strolling around cities you have never encountered before.",
                quest1: "Practice going on and off the bicycle",
                quest2: "Try to balance for one minute while scooting on one feet",
                quest3: "Watch adrenaline video without looking away to hone vision",
                25, 89, 102
            ));

        mockData.Add(SkillType.Piano,
            new Skill(SkillType.Piano, "Piano",
                "Relaxing melody. Revamping mind. Endless personalisation of the solo instrument. Turn your individuality into a new piece of music through playing piano.",
                quest1: "Crawl your hands for 5 minutes while listening to your favorite piano music",
                quest2: "Play C major musical scale",
                quest3: "Play C major cadence",
                55, 125, 115
            ));

        mockData.Add(SkillType.Basketball,
            new Skill(SkillType.Basketball, "Basketball",
                "Jump, soar and shoot. Dribble and possess the ball as if it is your bestie. Create your new basketball experience and have invariable sports time.",
                quest1: "Jump 10 times as high as you can",
                quest2: "Dribble the ball in the same position for five minutes",
                quest3: "Run for 2 km",
                77, 65, 100
            ));

        return mockData;
    }
}
