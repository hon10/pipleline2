using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{

    public enum Page
    {
        TitlePage, TermsAndConditionsPage, LoadingPage,
        SelectingPage, HubMenuPage, StorePage, CommunityPage
    }

    [SerializeField] private TitlePage _titlePage;
    [SerializeField] private TermsAndConditionsPage _termsAndConditionsPage;
    [SerializeField] private LoadingPage _loadingPage;
    [SerializeField] private SelectingPage _selectingPage;
    [SerializeField] private HubMenuPage _hubMenuPage;
    [SerializeField] private StorePage _storePage;
    [SerializeField] private CommunityPage _communityPage;

    void Start()
    {
        // Enter point
        _Init();
        _StartMainFlow();
    }

    private void _Init()
    {
        _loadingPage.Init();
        _selectingPage.Init();

        _hubMenuPage.Init();
    }

    private async void _StartMainFlow()
    {
        Debug.Log("START");

        while (true)
        {
            await _titlePage.Flow();

            await _termsAndConditionsPage.Flow();

            await _loadingPage.Flow();
            Debug.Log("_loadingPage DONE");

            var selectedSkills = await _selectingPage.Flow();
            Debug.Log($"_selectingPage DONE {selectedSkills.Count}");

            while (true)
            {
                var nextPage = await _hubMenuPage.Flow(selectedSkills);

                if (nextPage == Page.StorePage)
                {
                    await _storePage.Flow(selectedSkills);
                }
                else if (nextPage == Page.CommunityPage)
                {
                    await _communityPage.Flow();
                }
                else if (nextPage == Page.TitlePage)
                {
                    break;
                }
            }
            _Init();
        }
        // // for testing store page
        // var test = new List<SkillData.SkillType>();
        // test.Add(SkillData.SkillType.Drawing);
        // test.Add(SkillData.SkillType.Basketball);
        // test.Add(SkillData.SkillType.Piano);
        // test.Add(SkillData.SkillType.Cycling);

        // await _storePage.Flow(test);
    }
}
