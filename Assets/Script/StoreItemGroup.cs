using UnityEngine;
using UnityEngine.UI;
using TMPro;

using static StorePage;

public class StoreItemGroup : MonoBehaviour
{
    [SerializeField] private Image _icon;
    [SerializeField] private TextMeshProUGUI[] _name;
    [SerializeField] private Image[] _image;
    [SerializeField] private TextMeshProUGUI[] _price;

    public void Init(Sprite icon, StoreItem[] storeItems)
    {
        _icon.sprite = icon;

        for (int i = 0; i < storeItems.Length; i++)
        {
            var storeItem = storeItems[i];

            _name[i].text = storeItem.Name;
            _image[i].sprite = storeItem.Image;
            _price[i].text = storeItem.Price.ToString();
        }
    }
}
