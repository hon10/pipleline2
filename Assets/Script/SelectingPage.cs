using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using TMPro;

using static SkillData;

public class SelectingPage : MonoBehaviour
{
    private enum SelectingPageState
    {
        Default = 0, Detail = 1
    }

    private static readonly int STATE_KEY = Animator.StringToHash(@"State");

    [SerializeField] private PageRoot _pageRoot;

    [SerializeField] private SelectingItem _selectingItemPrefab;
    [SerializeField] private Transform _selectingItemsRoot;
    [SerializeField] private Button _toNextPhaseBtn;
    [SerializeField] private Animator _animator;

    [Header("Detail View")]
    [SerializeField] private TextMeshProUGUI _detailTitle;
    [SerializeField] private TextMeshProUGUI _detailDescription;
    [SerializeField] private Image _detailIcon;
    [SerializeField] private Image _radarChart;
    [SerializeField] private Button _detailBgBackBtn;
    [SerializeField] private Button _detailCloseBtn;

    [Header("Sprite")]
    [SerializeField] private Sprite _drawingIcon;
    [SerializeField] private Sprite _cyclingIcon;
    [SerializeField] private Sprite _pianoIcon;
    [SerializeField] private Sprite _basketballIcon;

    [SerializeField] private Sprite _drawingRadarChart;
    [SerializeField] private Sprite _cyclingRadarChart;
    [SerializeField] private Sprite _pianoRadarChart;
    [SerializeField] private Sprite _basketballRadarChart;

    [Header("Selecting Item")]
    [SerializeField] private SelectingItem _drawingSelectingItem;
    [SerializeField] private SelectingItem _cyclingSelectingItem;
    [SerializeField] private SelectingItem _pianoSelectingItem;
    [SerializeField] private SelectingItem _basketballSelectingItem;

    // private struct SelectingItem{
    //     Button
    // };

    // private HashSet<SkillType> _selectingItems = new HashSet<SkillType>();

    public void Init()
    {
        _animator.SetInteger(STATE_KEY, (int)SelectingPageState.Default);

        _detailBgBackBtn.onClick.AddListener(_CloseDetailView);
        _detailCloseBtn.onClick.AddListener(_CloseDetailView);

        var skillData = SkillData.MockData;

        _drawingSelectingItem.Init(() => _ShowDetailView(skillData[SkillType.Drawing]));
        _cyclingSelectingItem.Init(() => _ShowDetailView(skillData[SkillType.Cycling]));
        _pianoSelectingItem.Init(() => _ShowDetailView(skillData[SkillType.Piano]));
        _basketballSelectingItem.Init(() => _ShowDetailView(skillData[SkillType.Basketball]));

        // _SetupSelectingItems();
    }

    public async UniTask<List<SkillType>> Flow()
    {
        // _selectingItems = new HashSet<SkillType>();

        _pageRoot.Show();

        _SetState(SelectingPageState.Default);

        // completed
        await _toNextPhaseBtn.onClick.OnInvokeAsync(new System.Threading.CancellationToken());

        _pageRoot.Hide();

        // return _selectingItems.ToList();
        return _CollectSelectedSkillType();
    }

    private List<SkillType> _CollectSelectedSkillType()
    {
        var selectedSkillType = new List<SkillType>();

        if (_drawingSelectingItem.State == SelectingItem.SelectingItemState.Selected)
        {
            selectedSkillType.Add(SkillType.Drawing);
        }
        if (_cyclingSelectingItem.State == SelectingItem.SelectingItemState.Selected)
        {
            selectedSkillType.Add(SkillType.Cycling);
        }
        if (_pianoSelectingItem.State == SelectingItem.SelectingItemState.Selected)
        {
            selectedSkillType.Add(SkillType.Piano);
        }
        if (_basketballSelectingItem.State == SelectingItem.SelectingItemState.Selected)
        {
            selectedSkillType.Add(SkillType.Basketball);
        }

        return selectedSkillType;
    }

    // private List<SkillType> _CollectSelectedSkillType()
    // {
    //     var selectedSkillType = new List<SkillType>();

    //     foreach (var item in _selectingItems)
    //     {
    //         if (item.State == SelectingItem.SelectingItemState.Selected)
    //         {
    //             selectedSkillType.Add(item.SkillType);
    //         }
    //     }

    //     return selectedSkillType;
    // }

    // private void _SetupSelectingItems()
    // {
    //     _ClearSelectingItems();

    //     var skillData = SkillData.MockData;

    //     foreach (var skill in skillData)
    //     {
    //         var skillItem = Instantiate(_selectingItemPrefab, _selectingItemsRoot);

    //         skillItem.Init(skill.Key, skill.Value.Title, onDetailBtnClicked: () =>
    //         {
    //             _ShowDetailView(skill.Value);
    //         });

    //         _selectingItems.Add(skillItem);
    //     }
    // }

    private void _ShowDetailView(Skill skill)
    {
        _SetState(SelectingPageState.Detail);

        _detailTitle.text = skill.Title;
        _detailDescription.text = skill.Description;

        switch (skill.Type)
        {
            case SkillType.Drawing:
                _detailIcon.sprite = _drawingIcon;
                _radarChart.sprite = _drawingRadarChart;
                break;
            case SkillType.Cycling:
                _detailIcon.sprite = _cyclingIcon;
                _radarChart.sprite = _cyclingRadarChart;
                break;
            case SkillType.Piano:
                _detailIcon.sprite = _pianoIcon;
                _radarChart.sprite = _pianoRadarChart;
                break;
            case SkillType.Basketball:
                _detailIcon.sprite = _basketballIcon;
                _radarChart.sprite = _basketballRadarChart;
                break;
            default:
                break;
        }
    }

    private void _CloseDetailView()
    {
        _SetState(SelectingPageState.Default);
    }

    // private void _ClearSelectingItems()
    // {
    //     foreach (var item in _selectingItems)
    //         Destroy(item.gameObject);

    //     _selectingItems.Clear();
    // }

    private void _SetState(SelectingPageState state)
    {
        _animator.SetInteger(STATE_KEY, (int)state);
    }
}
