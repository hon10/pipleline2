using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using TMPro;

using static SkillData;
using static Controller;

public class HubMenuPage : MonoBehaviour
{
    private enum HubMenuPageState
    {
        PanalShow = 0, PanalShown = 1, PanalHide = 2, PanalHidden = 3
    }

    private static readonly int STATE_KEY = Animator.StringToHash(@"State");

    [SerializeField] private PageRoot _pageRoot;

    [SerializeField] private CanvasGroup _skillPanal;

    [Header("Sprite")]
    [SerializeField] private Sprite _drawingIcon;
    [SerializeField] private Sprite _cyclingIcon;
    [SerializeField] private Sprite _pianoIcon;
    [SerializeField] private Sprite _basketballIcon;

    [SerializeField] private Sprite _drawingRadarChart;
    [SerializeField] private Sprite _cyclingRadarChart;
    [SerializeField] private Sprite _pianoRadarChart;
    [SerializeField] private Sprite _basketballRadarChart;

    [Header("SkillContent")]
    [SerializeField] private Image _icon;
    [SerializeField] private Image _radarChart;
    [SerializeField] private TextMeshProUGUI _title;
    [SerializeField] private TextMeshProUGUI _description;
    [SerializeField] private TextMeshProUGUI _quest1;
    [SerializeField] private TextMeshProUGUI _quest2;
    [SerializeField] private TextMeshProUGUI _quest3;
    [SerializeField] private TextMeshProUGUI _bonus1;
    [SerializeField] private TextMeshProUGUI _bonus2;
    [SerializeField] private TextMeshProUGUI _bonus3;

    [Header("Buttons")]
    [SerializeField] private Button _drawingBtn;
    [SerializeField] private Button _cyclingBtn;
    [SerializeField] private Button _pianoBtn;
    [SerializeField] private Button _basketballBtn;
    [SerializeField] private Button _closeSkillBtn;

    [SerializeField] private Button _storeBtn;
    [SerializeField] private Button _communityBtn;
    [SerializeField] private Button _backToTitleBtn;

    [SerializeField] private Animator _animator;

    private Page _nextPage;

    public void Init()
    {
        _animator.SetInteger(STATE_KEY, (int)HubMenuPageState.PanalShow);

        _drawingBtn.onClick.AddListener(() =>
        {
            // _SetCanvasGroup(_skillPanal, true);
            _animator.SetInteger(STATE_KEY, (int)HubMenuPageState.PanalShow);
            _SetSkillPanelContent(SkillType.Drawing);
        });
        _cyclingBtn.onClick.AddListener(() =>
        {
            // _SetCanvasGroup(_skillPanal, true);
            _animator.SetInteger(STATE_KEY, (int)HubMenuPageState.PanalShow);
            _SetSkillPanelContent(SkillType.Cycling);
        });
        _pianoBtn.onClick.AddListener(() =>
        {
            // _SetCanvasGroup(_skillPanal, true);
            _animator.SetInteger(STATE_KEY, (int)HubMenuPageState.PanalShow);
            _SetSkillPanelContent(SkillType.Piano);
        });
        _basketballBtn.onClick.AddListener(() =>
        {
            // _SetCanvasGroup(_skillPanal, true);
            _animator.SetInteger(STATE_KEY, (int)HubMenuPageState.PanalShow);
            _SetSkillPanelContent(SkillType.Basketball);
        });

        _closeSkillBtn.onClick.AddListener(() =>
        {
            // _SetCanvasGroup(_skillPanal, false);
            _animator.SetInteger(STATE_KEY, (int)HubMenuPageState.PanalHide);
        });

        // To next page
        _backToTitleBtn.onClick.AddListener(() =>
        {
            _nextPage = Page.TitlePage;
        });
        _storeBtn.onClick.AddListener(() =>
        {
            _nextPage = Page.StorePage;
        });
        _communityBtn.onClick.AddListener(() =>
        {
            _nextPage = Page.CommunityPage;
        });
    }

    public async UniTask<Page> Flow(List<SkillType> skills)
    {
        _pageRoot.Show();

        _SetSkillButtons(skills);

        if (skills.Count > 0)
        {
            _SetSkillPanelContent(skills[0]);
            _animator.SetInteger(STATE_KEY, (int)HubMenuPageState.PanalShow);
            // _SetCanvasGroup(_skillPanal, true);
        }
        else
        {
            _animator.SetInteger(STATE_KEY, (int)HubMenuPageState.PanalHide);
            // _SetCanvasGroup(_skillPanal, false);
        }

        // completed
        // await _backToTitleBtn.onClick.OnInvokeAsync(new System.Threading.CancellationToken()) ;

        await UniTask.WhenAny(
            _backToTitleBtn.onClick.OnInvokeAsync(new System.Threading.CancellationToken()),
            _storeBtn.onClick.OnInvokeAsync(new System.Threading.CancellationToken()),
            _communityBtn.onClick.OnInvokeAsync(new System.Threading.CancellationToken())
        );

        _pageRoot.Hide();

        return _nextPage;
    }

    private void _SetCanvasGroup(CanvasGroup cg, bool show)
    {
        cg.interactable = show;
        cg.blocksRaycasts = show;
        cg.alpha = show ? 1 : 0;
    }

    private void _SetSkillButtons(List<SkillType> skills)
    {
        _drawingBtn.gameObject.SetActive(false);
        _cyclingBtn.gameObject.SetActive(false);
        _pianoBtn.gameObject.SetActive(false);
        _basketballBtn.gameObject.SetActive(false);

        foreach (var skillType in skills)
        {
            switch (skillType)
            {
                case SkillType.Drawing:
                    _drawingBtn.gameObject.SetActive(true);
                    break;
                case SkillType.Cycling:
                    _cyclingBtn.gameObject.SetActive(true);
                    break;
                case SkillType.Piano:
                    _pianoBtn.gameObject.SetActive(true);
                    break;
                case SkillType.Basketball:
                    _basketballBtn.gameObject.SetActive(true);
                    break;
                default:
                    break;
            }
        }
    }

    private void _SetSkillPanelContent(SkillType skillType)
    {
        var skillData = MockData[skillType];

        _title.text = skillData.Title;
        _description.text = skillData.Description;
        _quest1.text = skillData.Quests[0];
        _quest2.text = skillData.Quests[1];
        _quest3.text = skillData.Quests[2];
        _bonus1.text = skillData.Bonuses[0].ToString();
        _bonus2.text = skillData.Bonuses[1].ToString();
        _bonus3.text = skillData.Bonuses[2].ToString();

        switch (skillType)
        {
            case SkillType.Drawing:
                _icon.sprite = _drawingIcon;
                _radarChart.sprite = _drawingRadarChart;
                break;
            case SkillType.Cycling:
                _icon.sprite = _cyclingIcon;
                _radarChart.sprite = _cyclingRadarChart;
                break;
            case SkillType.Piano:
                _icon.sprite = _pianoIcon;
                _radarChart.sprite = _pianoRadarChart;
                break;
            case SkillType.Basketball:
                _icon.sprite = _basketballIcon;
                _radarChart.sprite = _basketballRadarChart;
                break;
            default:
                break;
        }
    }
}
