using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using static SkillData;

public class SelectingItem : MonoBehaviour
{
    public enum SelectingItemState
    {
        Unselected = 0, Selected = 1
    }

    private static readonly int STATE_KEY = Animator.StringToHash(@"State");

    // [SerializeField] private TextMeshProUGUI _title;
    [SerializeField] private Button _selectBtn;
    [SerializeField] private Button _detailBtn;
    [SerializeField] private Button _unselectBtn;
    [SerializeField] private Animator _animator;

    [SerializeField] private SkillType _skillType;

    // private SkillType _skillType;
    public SkillType SkillType => _skillType;

    private SelectingItemState _state = SelectingItemState.Unselected;
    public SelectingItemState State => _state;

    public void Init(
        // SkillType skillType,
        // string title, 
        Action onDetailBtnClicked)
    {
        // _skillType = skillType;
        // _title.text = title;


        _selectBtn.onClick.AddListener(_Select);
        _detailBtn.onClick.AddListener(() => onDetailBtnClicked.Invoke());
        _unselectBtn.onClick.AddListener(_Unselect);

        _Unselect();
    }

    private void _Select()
    {
        Debug.Log("Select");
        _SetState(SelectingItemState.Selected);
    }

    private void _Unselect()
    {
        Debug.Log("Unselect");
        _SetState(SelectingItemState.Unselected);
    }

    private void _SetState(SelectingItemState state)
    {
        _state = state;
        _animator.SetInteger(STATE_KEY, (int)state);
    }
}
