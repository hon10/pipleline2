using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;

public class TermsAndConditionsPage : MonoBehaviour
{
    // private enum LoadingPageState
    // {
    //     Default = 0, Loading = 1, Completed = 2
    // }

    // private static readonly int STATE_KEY = Animator.StringToHash(@"State");

    [SerializeField] private PageRoot _pageRoot;

    [SerializeField] private Button _toNextPhaseBtn;
    // [SerializeField] private Animator _animator;

    public void Init()
    {
    }

    public async UniTask Flow()
    {
        _pageRoot.Show();

        // completed
        await _toNextPhaseBtn.onClick.OnInvokeAsync(new System.Threading.CancellationToken());

        _pageRoot.Hide();
    }
}
